<!DOCTYPE html>

<html>

<head>

    <? include 'includes/head.php'; ?>
    
    <title>Boostwagen :: Каталог</title>

    <link rel="stylesheet" href="/css/catalog-page.css">

    <script src="/js/catalog-page.js"></script>

</head>

<body id="catalog-page" class="body not-authorised">

<? include 'includes/header.php'; ?>

<div id="main" class="wrap">

    <div class="grid">

        <? include 'includes/catalog-menu.php'; ?>

        <main id="catalog-page-content" class="three-quarters unit">

            <div id="catalog-sections-list">

                <div class="grid equalised">

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Автосцепка (СА-3) и комплектующие</h2>
                                <img src="/upload/catalog/ca-3.png" alt="Автосцепка (СА-3) и комплектующие">
                            </div>

                            <div class="items" data-height="">
                                <ul class="list">

                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замок автосцепки
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замок автосцепки
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замкодержатель
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подъёмник замка
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Предохранитель замка
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замкодержатель
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подъёмник замка
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Предохранитель замка
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Детали автосцепного устройства</h2>
                                <img src="/upload/catalog/asd-details.png" alt="Детали автосцепного устройства">
                            </div>

                            <div class="items">
                                <ul class="list">

                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Хомут тяговый
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Клин тягового хомута
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Балочка центрирующая
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подвеска маятниковая
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Рычаг расцепного привода
                                                <span>3054.35.00.050</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Метизы</h2>
                                <img src="/upload/catalog/bolt.png" alt="Метизы">
                            </div>

                            <div class="items">
                                <ul class="list">
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Болт ГОСТ 7798-70
                                                <span>3054.35.00.050</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Гайка ГОСТ 5915-70
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Болт стяжной
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Болт стяжной
                                                <span>3054.35.00.050</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Болт стяжной с гайкой
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <div class="grid equalised">

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Автосцепка (СА-3) и комплектующие</h2>
                                <img src="/upload/catalog/asd-details.png" alt="Автосцепка (СА-3) и комплектующие">
                            </div>

                            <div class="items">
                                <ul class="list">

                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замок автосцепки
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замкодержатель
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подъёмник замка
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Предохранитель замка
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Автосцепка (СА-3) и комплектующие</h2>
                                <img src="/upload/catalog/asd-details.png" alt="Автосцепка (СА-3) и комплектующие">
                            </div>

                            <div class="items">
                                <ul class="list">

                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замок автосцепки
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замкодержатель
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подъёмник замка
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Предохранитель замка
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                    <section class="one-third unit">

                        <div class="wrapper">

                            <div class="header">
                                <h2>Автосцепка (СА-3) и комплектующие</h2>
                                <img src="/upload/catalog/asd-details.png" alt="Автосцепка (СА-3) и комплектующие">
                            </div>

                            <div class="items">
                                <ul class="list">

                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замок автосцепки
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Замкодержатель
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Подъёмник замка
                                                <span>ЧЛЗ.ПКБ.ЦВ-106.00.001-1.-2ЛУ</span>
                                            </a>
                                        </h3>
                                    </li>
                                    <li>
                                        <h3>
                                            <a href="/catalog-item.php">
                                                Предохранитель замка
                                                <span>106.01.000-0-05СБ</span>
                                            </a>
                                        </h3>
                                    </li>

                                </ul>

                                <div class="footer">
                                    <div class="controls">
                                        <a href="#" class="show">Показать все детали</a>
                                        <a href="#" class="hide">Скрыть детали</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </section>

                </div>

            </div>

        </main>

    </div>

</div>

<? include 'includes/footer.php'; ?>

</body>

</html>