<!DOCTYPE html>

<html>

    <head>
    
        <? include 'includes/head.php'; ?>

        <title>Boostwagen :: Главная</title>

    </head>

    <body id="front-page" class="body not-authorised">

        <? include 'includes/header.php'; ?>

        <? include 'includes/front-slider.php'; ?>

        <div id="main" class="wrap">

            <link rel="stylesheet" href="/css/front-navigation.css">

            <main id="front-navigation" class="grid">

                <div class="three-quarters unit">

                    <div class="grid">

                        <div id="online" class="one-third unit">
                            <div class="pointer up">
                                <h2>
                                    <a href="/online/">
                                        <span class="underline">Boostwagen</span><br>
                                        <span class="underline">online</span>
                                    </a>
                                </h2>
                                <p>Интернет-магазин железнодорожных запчастей.</p>
                            </div>
                        </div>

                        <div id="storage-logistics" class="one-third unit">
                            <div class="pointer down">
                                <h2>
                                    <a href="/storage-logistics/">
                                        <span class="underline">Складская</span><br>
                                        <span class="underline">логистика</span>
                                    </a>
                                </h2>
                                <p>Хранение крупногоабаритных грузов на любой срок.</p>
                            </div>
                        </div>

                        <div id="transport-logistics" class="one-third unit">
                            <div class="pointer right">
                                <h2>
                                    <a href="/transport-logistics/">
                                        <span class="underline">Транспортная</span><br>
                                        <span class="underline">логистика</span>
                                    </a>
                                </h2>
                                <p>Доставка грузов контейнерами, авто- и ж/д траспортом.</p>
                            </div>
                        </div>

                    </div>

                    <div class="grid">

                        <div id="bw-model" class="one-third unit">
                            <div class="pointer top-right-arrow">
                                <h3><a href="/about/bw-model/"><span class="underline">BW-модель</span></a></h3>
                                <p>Система профессионального управления закупками железнодорожной продукции для содержания и
                                    обслуживания подвижного состава</p>
                            </div>
                        </div>

                        <div id="storage" class="two-thirds unit">
                            <div class="pointer">
                                <div class="caption">
                                    <h3>Ответственное хранение и обработка грузов</h3>
                                    <a href="/storage/" class="button transparent wide">Стоимость хранения</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="status" class="one-quarter unit">

                    <div class="grid">
                        <div class="whole unit">
                            <div class="pointer">
                                <div class="caption">
                                    <h3>Статус заказа</h3>
                                    <form action="" class="form">
                                        <div class="field">
                                            <input type="text" placeholder="Номер заказа">
                                            <p><a href="/order/remind/">Забыли номер?</a></p>
                                        </div>
                                        <div class="actions">
                                            <input type="submit" class="button transparent wide" value="Расчёт доставки">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </main>

        </div>

        <? include 'includes/footer.php'; ?>

    </body>

</html>