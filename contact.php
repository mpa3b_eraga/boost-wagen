<!DOCTYPE html>

<html>

<head>

    <? include 'includes/head.php'; ?>
    
    <title>Boostwagen :: Контакты</title>
    
    <link rel="stylesheet" href="/css/contact-page.css">

    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script src="/js/contact-page.js"></script>

</head>

<body id="contact-page" class="body not-authorised">

<? include 'includes/header.php'; ?>

<div id="main" class="wrap">

    <main id="contact-page-content" class="grid">
        <div class="whole unit">

            <div class="shadowed">

                <div class="wrapper">

                    <h1>Контакты</h1>

                    <menu id="contact-page-menu">
                        <a href="#contact-page-head-office" class="active">Головной офис</a>
                        <a href="#contact-page-storage">Склад</a>
                    </menu>

                    <div id="contact-page-tabs">

                        <section id="contact-page-head-office" class="active">

                            <div class="grid">
                                <div class="half unit">
                                    <h4 class="title">Boostwagen Group</h4>
                                </div>
                                <div class="half unit align-right">
                                    <p class="title">
                                        <a href="javascript:window.print();">Распечатать схему проезда</a>
                                    </p>
                                </div>
                            </div>

                            <div class="grid">
                                <div class="one-third unit">
                                    <p>Нижний Новгород, ул. Коминтерна, 139 <br>
                                        <a href="mailto:info@boostwagen.ru">info@boostwagen.ru</a>
                                    </p>
                                    <p>Это первый блок.</p>
                                </div>
                                <div class="one-third unit">
                                    <p>+7 495 123-45-66 — секретарь <br>
                                        +7 495 123-45-67 — заказы <br>
                                        +7 495 123-45-68 — факс</p>
                                </div>
                            </div>

                        </section>

                        <section id="contact-page-storage">

                            <div class="grid">
                                <div class="half unit">
                                    <h4 class="title">Складская зона Boostwagen Group</h4>
                                </div>
                                <div class="half unit align-right">
                                    <p class="title">
                                        <a href="javascript:window.print();">Распечатать схему проезда</a>
                                    </p>
                                </div>
                            </div>

                            <div class="grid">
                                <div class="one-third unit">
                                    <p>Нижний Новгород, ул. Коминтерна, 139 <br>
                                        <a href="mailto:info@boostwagen.ru">info@boostwagen.ru</a>
                                    </p>
                                    <p>Это второй блок.</p>
                                </div>
                                <div class="one-third unit">
                                    <p>+7 495 123-45-66 — секретарь <br>
                                        +7 495 123-45-67 — заказы <br>
                                        +7 495 123-45-68 — факс</p>
                                </div>
                            </div>

                        </section>

                    </div>

                </div>

                <div id="location-map" class="with-shadow"></div>

            </div>

        </div>
    </main>

</div>

<? include 'includes/footer.php'; ?>

</body>

</html>