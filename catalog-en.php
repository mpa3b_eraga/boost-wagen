<!DOCTYPE html>

<html>

<head>
    
    <? include 'includes/head.php'; ?>
    
    <title>Boostwagen :: Catalogue</title>

    <link rel="stylesheet" href="/css/catalog-page.css">
    <link rel="stylesheet" href="/css/catalog-en.css">

    <script src="/lib/mask/jquery.mask.js"></script>
    <script src="/js/catalog-page.js"></script>

</head>

<body id="catalog-page" class="body not-authorised">

<? include 'includes/header.php'; ?>

<div id="main" class="wrap">

    <div class="grid">

        <main id="catalog-page-content" class="three-quarters unit">

            <div class="wrapper">

                <h1>Catalogue</h1>

                <section id="catalog-page-catalog">

                    <p>To order railway car spare parts please download the catalogue, single out the parts you'd
                        like to order, <br>
                        then fill a short application form and our manager will contact you.</p>
    
                    <div id="catalog-page-catalog-download" class="grid as-table">

                        <div class="one-fifth unit">

                            <img src="/img/icon-pdf.png" alt="PDF catalogue">

                        </div>

                        <div class="four-fifths unit">
    
                            <h2><a href="/upload/catalog.pdf" download="Boostwagen Catalog.pdf">Boostwagen
                                    production catalogue</a></h2>
                            <p>size 1.4 MB</p>

                        </div>

                    </div>

                </section>

                <section id="catalog-page-process">

                    <h2>How it works</h2>

                    <ol>
                        <li>
                            <p>Download the catalog</p>
                        </li>
                        <li>
                            <p>Single out spare parts in it</p>
                        </li>
                        <li>
                            <p><a href="#catalog-page-application" class="show-form">Fill in an application form and
                                    attach the filled catalogue</a></p>

                            <div id="catalog-page-application" class="hidden">

                                <form action="" id="application-form" class="form">

                                    <div class="field">
                                        <label for="application-form-company">Company name</label>
                                        <input type="text" name="application-form-company"
                                               id="application-form-company"
                                               placeholder="">
                                    </div>

                                    <div class="field">
                                        <label for="application-form-name">Your name</label>
                                        <input type="text" name="application-form-name" id="application-form-name"
                                               placeholder="">
                                    </div>

                                    <div class="field">
                                        <label for="application-form-comment">Your contacts and order</label>
                                        <textarea name="application-form-comment" id="application-form-comment"
                                                  placeholder=""></textarea>
                                    </div>

                                    <div class="field">
                                        <input type="text" name="application-form-file">
                                        <button name="application-form-browse">Обзор</button>
                                    </div>

                                    <div class="actions">

                                        <p>Pleas, fill in yout name and your contacts and order.</p>

                                        <input type="submit" value="Send →">

                                    </div>

                                </form>

                            </div>

                            <div id="catalog-page-application-sent" class="hidden">
                                <p><em>The application was sent successfully!</em></p>
                            </div>

                        </li>
                        <li>
                            <p>We e-mail you or call at the time appointed in the form</p>
                        </li>
                    </ol>

                </section>

            </div>

        </main>

        <aside id="catalog-page-samples" class="one-quarter unit">

            <div class="wrapper">

                <h2 class="title">Examples <br> of production</h2>

                <ul class="items">

                    <li class="item">
                        <img src="/upload/catalog/samples/as-106.png" alt="">
                        <h3>Automatic coupling</h3>
                    </li>

                    <li class="item">
                        <img src="/upload/catalog/samples/triangle.png" alt="">
                        <h3>Triangle</h3>
                    </li>

                    <li class="item">
                        <img src="/upload/catalog/samples/break.png" alt="">
                        <h3>Brake system working space</h3>
                    </li>

                </ul>

                <p>
                    <a href="/upload/catalog.pdf" download="Boostwagen Catalog.pdf">Download the catalogue</a>
                    to see the whole range of products.
                </p>

            </div>

        </aside>

    </div>

</div>

<? include 'includes/footer.php'; ?>

</body>

</html>