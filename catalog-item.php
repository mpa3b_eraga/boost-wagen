<!DOCTYPE html>

<html>

<head>
    
    <? include 'includes/head.php'; ?>
    
    <title>Boostwagen :: Страница товара</title>

    <link rel="stylesheet" href="/css/catalog-item.css">

    <link rel="stylesheet" href="/lib/slick/slick.css">
    <script src="/lib/slick/slick.js"></script>

    <link rel="stylesheet" href="/lib/colorbox/colorbox.css">
    <script src="/lib/colorbox/jquery.colorbox.js"></script>

    <script src="/js/catalog-item.js"></script>

</head>

<body id="catalog-item-page" class="body not-authorised">

<? include 'includes/header.php'; ?>

<div id="main" class="wrap">

    <div class="grid">

        <? include 'includes/catalog-menu.php'; ?>

        <main id="catalog-item-page-content" class="three-quarters unit">

            <div class="wrapper">

                <header class="grid">

                    <h1>Автосцепка (СА-3)</h1>
                    <h2>106.01.000-0-05СБ</h2>

                </header>

                <section class="grid">

                    <div class="two-thirds unit">

                        <div id="catalog-item-image">
                            <div class="slide">
                                <img src="/upload/catalog-item/as-3-1.jpg" alt="Автосцепка СА-3">
                            </div>
                            <div class="slide">
                                <img src="/upload/catalog-item/as-3-2.jpg" alt="Автосцепка СА-3">
                            </div>
                        </div>

                        <div id="catalog-item-details" class="grid">

                            <div id="catalog-item-details-vendor" class="half unit">
                                <h6>Производитель</h6>
                                <p>ОАО «НПК Уралвагонзавод»</p>
                            </div>

                            <div id="catalog-item-details-weight" class="half unit">
                                <h6>Вес</h6>
                                <p>213 кг</p>
                            </div>

                        </div>

                    </div>

                    <div class="one-third unit">

                        <div id="catalog-item-images" class="slider-nav">
                            <a href="#">
                                <img src="/upload/catalog-item/image-1.png" alt="Автосцепка (СА-3) фото 1">
                            </a>
                            <a href="#">
                                <img src="/upload/catalog-item/image-2.png" alt="Автосцепка (СА-3) фото 2">
                            </a>
                        </div>

                        <div id="catalog-item-price">

                            <p class="date">На 15 апреля 2015 года</p>

                            <p class="price">
                                <span class="value">24 000<i class="rouble"></i></span>
                            </p>

                            <p class="price-w-tax">
                                <span class="value">27 000<i class="rouble"></i></span> с НДС
                            </p>

                        </div>

                        <div id="catalog-item-buy">

                            <form action="" id="catalog-item-buy-form">

                                <div class="wrapper">

                                    <input type="text" name="catalog-item-buy-form-quantity" value="1 шт.">

                                    <button class="plus control" name="catalog-item-buy-form-quantity-plus">+</button>
                                    <button class="minus control" name="catalog-item-buy-form-quantity-minus">-</button>

                                </div>

                                <button type="submit">Заказать</button>

                            </form>

                        </div>

                    </div>

                </section>

                <aside id="catalog-item-other-items" class="grid">

                    <div class="whole unit">

                        <h3>Другие товары раздела «Автосцепка СА-3 и комплектующие»</h3>

                        <div class="grid">

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-1.png">
                                    <h4>Замок от автосцепки <br>
                                        <span>106.01.002-1</span></h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-2.png">
                                    <h4>Замкодержатель <br>
                                        <span>106.01.003-0</span>
                                    </h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-3.png">
                                    <h4>Валик подъёмника <br>
                                        <span>106.01.017-0</span></h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-4.png">
                                    <h4>Цепт расцепного привода (17 звеньев) <br>
                                        <span>106.01.010-0</span></h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-5.png">
                                    <h4>Предохранитель замка <br>
                                        <span>106.01.006-1</span></h4>
                                </a>
                            </div>

                        </div>
                        <div class="grid">

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-6.png">
                                    <h4>Цепь блокировочная расцепного привода (22 звена) <br>
                                        <span>572.01.011-0</span></h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-7.png">
                                    <h4>Ограничитель <br>
                                        <span>M1761.001</span></h4>
                                </a>
                            </div>

                            <div class="one-fifth unit">
                                <a href="#">
                                    <img src="/upload/catalog-item/other/item-8.png">
                                    <h4>Плечо расцепного рычага <br>
                                        <span>M1761.001</span></h4>
                                </a>
                            </div>

                        </div>

                    </div>

                </aside>

            </div>

        </main>

    </div>

</div>

<aside class="hidden">
    <div id="catalog-item-buy-confirmation">

        <h3 class="title">Вы добавили в корзину</h3>

        <img src="/upload/catalog-item/image-1.png" alt="">
    
        <div id="catalog-item-buy-confirmation-details" class="details grid no-gutters">
            <div class="half unit">
                <h6>
                    <span id="item"></span>
                    <br>
                    <span id="model"></span>
                </h6>
            </div>
            <div class="quantity half unit">
                <p>
                    Количество: <span id="catalog-item-buy-confirmation-quantity" class="value"></span>
                </p>
            </div>
        </div>


        <p>
            <a href="#" class="button transparent close">Продолжить покупки</a>
            <a href="/checkout/" class="button">Оформить заказ</a>
        </p>

    </div>
</aside>

<? include 'includes/footer.php'; ?>

</body>

</html>