/**
 * Created by ilya_ on 09.06.2016.
 */

$(document).on(
    'ready',
    function () {
    
        var frontSlider = $('.slider', '#front-slider'),
            innerSlider = $('#inner-slider', '#front-slider');
    
        frontSlider.slick(
            {
                speed: 500,
                autoplay: true,
                autoplaySpeed: 2500,
                slidesToShow: 1,
                slidesToScroll: 1,
                cssEase: 'ease',
                fade: true,
                dots: true,
                arrows: true,
                prevArrow: '<a href="#" class="prev"></a>',
                nextArrow: '<a href="#" class="next"></a>'
            }
        );
    
        innerSlider.slick(
            {
                speed: 500,
                initialSlide: 1,
                autoplaySpeed: 2000,
                slidesToShow: 3,
                slidesToScroll: 1,
                cssEase: 'ease',
                dots: false,
                arrows: false,
                centerMode: true,
                centerPadding: 0,
                infinite: false
            }
        );
    
        frontSlider.on(
            'afterChange',
            function () {
    
                if (innerSlider.closest('.slick-active').length > 0) {
    
                    innerSlider.slick('slickPlay');
                    innerSlider.slick('slickNext');
    
                    innerSlider.on(
                        'edge',
                        function () {
                            innerSlider.slick('slickStop');
                            frontSlider.slick('slickPlay');
                            innerSlider.slickGoTo(1);
                        }
                    );
                }
    
            }
        );
    
    }
);
