$(document).on(
    'ready',
    function () {
    
        $('ul.menu > li > a', '#catalog-menu').on(
            'mouseover',
            function () {
                $(this).parent('li').children('ul').fadeIn();
            }
        );
    
        $('ul.menu > li', '#catalog-menu').on(
            'mouseleave',
            function () {
                $(this).children('ul').fadeOut();
            }
        );
    
        $('li.section > a', 'ul.menu', '#catalog-menu').on(
            'click',
            function (event) {
    
                event.preventDefault();
    
                var container = $(this).closest('li');
    
                if (container.hasClass('collapsed')) {
                    container.find('ul').slideDown();
                    container.removeClass('collapsed').addClass('expanded');
                }
                else {
    
                    if (container.hasClass('expanded')) {
                        container.find('ul').slideUp();
                        container.removeClass('expanded').addClass('collapsed');
                    }
                    
                }
    
            }
        );
    
    }
);

$(document).on(
    'ready',
    function () {

        $('.frame', '#sidebar-left').stick_in_parent(
            {
                parent: $('#main > .grid'),
                sticky_class: 'sticky',
                offset_top: parseInt($('#main').css('paddingTop'))
            }
        );
        
    }
);