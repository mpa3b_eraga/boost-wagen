$(document).on(
    'ready',
    function () {

        $('input').on(
            'input',
            function () {

                var value = $(this).val();

                if (value.length > 0) {
                    $(this).removeClass('italic').addClass('normal');
                }
                else {
                    $(this).removeClass('normal').addClass('italic');
                }

            }
        );
        
    }
);
