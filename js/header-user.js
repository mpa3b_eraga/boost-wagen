$(document).on(
    'ready',
    function () {

        $('a[href="#header-user-dropdown"]').on(
            'click',
            function (event) {

                event.preventDefault();

                $('.dropdown', '#header-user').fadeOut();

                $('#header-user-dropdown').fadeIn();

                $(document).on(
                    'mousedown',
                    function (event) {

                        if (!$(event.target).closest('#header-user-dropdown').length > 0) {
                            $('#header-user-dropdown').fadeOut();
                            $(this).unbind('click');
                        }

                    }
                );


            }
        );

        $('a[href="#header-guest-dropdown"]').on(
            'click',
            function (event) {

                event.preventDefault();

                $('.dropdown', '#header-user').fadeOut();

                $('#header-guest-dropdown').fadeIn();

                $(document).on(
                    'mousedown',
                    function (event) {

                        if (!$(event.target).closest('#header-guest-dropdown').length > 0) {
                            $('#header-guest-dropdown').fadeOut();
                            $(this).unbind('click');
                        }

                    }
                );


            }
        );

        $('form', '#header-user').on(
            'submit',
            function (event) {

                event.preventDefault();

                $(this).closest('.dropdown').fadeOut();

            }
        );
    }
);