$(document).on(
    'ready',
    function () {

        $('a', '.wrapper', '#partners-list').on(
            'click',
            function (event) {

                event.preventDefault();
                $(this).closest('.unit').find('.caption').fadeIn();

            }
        );

        $('a.close', '.caption', '.wrapper', '#partners-list').on(
            'click',
            function (event) {

                event.preventDefault();
                $(this).closest('.caption').fadeOut();

            }
        );

    }
);