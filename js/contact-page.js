$(document).on(
    'ready',
    function () {

        var container = $('#contact-page-tabs'),
            menu = $('#contact-page-menu');

        $('a', menu).on(
            'click',
            function (event) {

                event.preventDefault();

                $('a', menu).removeClass('active');
                $(this).addClass('active');

                var id = $(this).attr('href');

                $('section', container).hide();
                $(id, container).show();

            }
        );

        ymaps.ready(

            function () {

                // улица Коминтерна, 139
                // Нижний Новгород, Россия
                // Широта 56°21′14″N (56.353769)
                // Долгота 43°52′2″E (43.867251)

                var locationMap = new ymaps.Map(
                    "location-map",
                    {
                        center: [56.353769, 43.867251],
                        zoom: 17,
                        controls: []
                    }
                    ),

                    Boostwagen  = new ymaps.Placemark(
                        locationMap.getCenter(),
                        {
                            hintContent: 'Главный офис Boostwagen'
                        },
                        {
                            iconLayout: 'default#image',
                            iconImageHref: '/img/map-location-marker.png',
                            iconImageSize: [40, 42],
                            iconImageOffset: [-3, -21]
                        }
                    );
    
                locationMap.geoObjects.add(Boostwagen);

            }
        );

    }
);
