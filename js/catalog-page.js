$(document).on(
    'ready',
    function () {

        $('a', '.footer', '.unit', '#catalog-sections-list').on(
            'click',
            function (event) {

                event.preventDefault();

                var items = $(this).parents('.wrapper').find('.items'),
                    list = items.find('ul.list'),
                    height = list.height();


                if ($(this).hasClass('show')) {

                    $(this).hide()
                        .parent().find('a.hide').css(
                        {
                            display: 'block'
                        }
                    );

                    items.data('height', height);

                    // TODO : не срабатывает присваивание и получение

                    items.animate(
                        {
                            height: height
                        }
                    );

                }
                else {

                    $(this).hide().parent().find('a.show').show();

                    items.animate(
                        {
                            height: '18em'
                        }
                    );

                }

            }
        );

    }
);

// EN

$(document).on(
    'ready',
    function () {

        $('a[href="#catalog-page-application"]').on(
            'click',
            function (event) {

                event.preventDefault();

                $('#catalog-page-application').slideDown();
                $('#catalog-page-application-sent').hide();

                $('form#application-form').on(
                    'submit',
                    function (event) {

                        event.preventDefault();

                        $('#catalog-page-application').slideUp();
                        $('#catalog-page-application-sent').show();
                    }
                )

            }
        );
    
        if ($('.wrapper', '#catalog-page-content').height() < $('.wrapper', '#catalog-page-samples').height()) {
            $('.wrapper', '#catalog-page-content').height($('.wrapper', '#catalog-page-samples').height());
        }

    }
);