$(document).on(
    'ready',
    function () {

        $('input', '#subscription-form').on(
            'focus',
            function () {
                $(this).parent('.field').addClass('focus');
            }
        );

        $('input', '#subscription-form').on(
            'blur',
            function () {
                $(this).parent('.field').removeClass('focus');
            }
        );

    }
);