$(document).on(
    'ready',
    function () {

        $('input#career-page-form-phone').mask('+7 999 999-99-99');
        $('input#vacancy-form-phone').mask('+7 999 999-99-99');
        $('input#contact-form-phone').mask('+7 999 999-99-99');

        $('a', '.career-page-menu', '#career-page').on(
            'click',
            function (event) {

                event.preventDefault();

                $(this).parent().find('a').removeClass('active');
                $(this).addClass('active');

            }
        );

        $('a[href="#show-form"]', '.vacancy', '#career-page-vacancies').on(
            'click',
            function (event) {

                event.preventDefault();

                $(this).closest('.vacancy').find('.resume').slideDown();

            }
        );

        $('a.hide', '.resume').on(
            'click',
            function (event) {

                event.preventDefault();

                $(this).closest('.resume').hide();

            }
        );

        $('a', '.controls', '.vacancy', '#career-page').on(
            'click',
            function (event) {

                event.preventDefault();

                var vacancy = $(this).closest('.vacancy');

                $(this).hide();

                if ($(this).hasClass('show')) {
                    $('.description', vacancy).show();
                    $('a.hide', vacancy).show();
                    vacancy.addClass('expanded');
                }
                else {
                    $('.description', vacancy).hide();
                    $('a.show', vacancy).show();
                    vacancy.removeClass('expanded');
                }

            }
        );

        $('.vacancy-form').on(
            'submit',
            function (event) {

                event.preventDefault();

                $.colorbox(
                    {
                        inline: true,
                        href: $('.wrapper', '.confirmation', this)
                    }
                );

            }
        );

        $('a', '#contact-form-show').on(
            'click',
            function (event) {

                event.preventDefault();

                $('#contact-form-show').hide();
                $('#contact-form-container').fadeIn();

            }
        );

        $('form', '#contact-form-container').on(
            'submit',
            function (event) {

                event.preventDefault();

                $('#contact-form-container').hide();
                $('#contact-form-confirmation').show();

            }
        );

    }
);
