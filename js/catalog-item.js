$(document).on(
    'ready',
    function () {

            $('#catalog-item-image').slick(
                {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        cssEase: 'ease',
                        fade: true,
                        dots: false,
                        arrows: false
                }
            );
        
            $('button.control', 'form#catalog-item-buy-form').on(
                'click',
                function (event) {

                    var input = $('input[name="catalog-item-buy-form-quantity"]', 'form#catalog-item-buy-form'),
                        quantity = parseInt(input.val());


                    event.preventDefault();

                    if($(this).hasClass('plus')) {
                        quantity = quantity + 1;
                    }
                    if($(this).hasClass('minus')) {
                        if(quantity > 1) {
                            quantity = quantity - 1;
                        }
                    }

                    input.val(quantity + ' шт.');

                }
            );

            $('a', '#catalog-item-images').on(
                'click',
                function (event) {

                        event.preventDefault();

                        var index = $(this).index();
                    
                        $(this).parent().find('a').removeClass('active');

                        $(this).addClass('active');

                        $('#catalog-item-image').slick('slickGoTo', index);

                }
            );

        $('form#catalog-item-buy-form').on(
            'submit',
            function (event) {

                event.preventDefault();
    
                var form     = $('#catalog-item-buy-confirmation'),
                    item     = $('h1', 'header', '#catalog-item-page-content').text(),
                    model    = $('h2', 'header', '#catalog-item-page-content').text(),
                    quantity = parseInt($('input[name="catalog-item-buy-form-quantity"]', this).val());
    
                $('#item', form).text(item);
                $('#model', form).text(model);
    
                $('#catalog-item-buy-confirmation-quantity', form).text(quantity);

                $.colorbox(
                    {
                        inline: true,
                        href: form,
                        minWidth: '22em',
                        scrolling: false
                    }
                );
    
            }
        );

        $('a.close', '#colorbox').on(
            'click',
            function () {
                $.colorbox.close();
            }
        );

    }
);