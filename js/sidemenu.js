$(document).on(
    'ready',
    function () {

        $('a.control', '#side-menu').on(
            'click',
            function (event) {

                event.preventDefault();

                $('#side-menu-bar').animate(
                    {
                        right: 0
                    }
                );
            }
        );

        $('a.close', '#side-menu-bar', '#side-menu').on(
            'click',
            function (event) {

                event.preventDefault();

                $('#side-menu-bar').animate(
                    {
                        right: '-100%'
                    }
                );

            }
        );

        $(document).on(
            'mouseup',
            function (event) {

                event.stopPropagation();

                $('#side-menu-bar').animate(
                    {
                        right: '-100%'
                    }
                );

            }
        );

    }
);