$(document).on(
    'ready',
    function () {

        $('a[href="#header-cart-dropdown"]').on(
            'click',
            function (event) {

                event.preventDefault();

                $('#header-cart-dropdown').fadeIn();
    
                $('#header-cart-dropdown').bind(
                    'mousewheel DOMMouseScroll',
                    function (e) {
            
                        var scrollTo = null;
            
                        if (e.type == 'mousewheel') {
                            scrollTo = (
                            e.originalEvent.wheelDelta * -1
                            );
                        }
            
                        else if (e.type == 'DOMMouseScroll') {
                            scrollTo = 40 * e.originalEvent.detail;
                        }
            
                        if (scrollTo) {
                            e.preventDefault();
                            $(this).scrollTop(scrollTo + $(this).scrollTop());
                        }
                    }
                );
                
                $(document).on(
                    'mousedown',
                    function (event) {

                        if (!$(event.target).closest('#header-cart-dropdown').length > 0) {
    
                            $('#header-cart-dropdown').fadeOut();
    
                            $(this).unbind('click');
    
                        }

                    }
                );

            }
        );

        $('ul.items', '#header-cart-dropdown').niceScroll(
            {
                cursorborder: 'none',
                cursorcolor: '7d8eb1',
                autohidemode: false,
                nativeparentscrolling: false
            }
        );

    }
);