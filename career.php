<!DOCTYPE html>

<html>

<head>
    
    <? include 'includes/head.php'; ?>
    
    <title>Boostwagen :: Карьера</title>

    <link rel="stylesheet" href="/css/career-page.css">

    <link rel="stylesheet" href="/lib/colorbox/colorbox.css">
    <script src="/lib/colorbox/jquery.colorbox.js"></script>

    <script src="/lib/mask/jquery.mask.js"></script>

    <script src="/js/career-page.js"></script>

</head>

<body id="career-page" class="body not-authorised">

<? include 'includes/header.php'; ?>

<div id="main" class="wrap">

    <div class="grid">

        <main id="career-page-content" class="three-quarters unit">

            <div class="wrapper">
                <h1>Карьера</h1>
            </div>

            <section id="career-page-content-advantages" class="wrapper">

                <div class="description">
                    <p>Boostwagen объединяет профессионалов, чьи главные качества – высокая ответственность и
                        умение
                        решать сложные задачи. Присоединяйтесь к нашей команде!</p>
                </div>

                <h2>Преимущества работы</h2>

                <table class="wide">
                    <tr>
                        <td id="advantage-adaptation" class="cell">
                            <h3>Система адаптации для новых сотрудников</h3>
                        </td>
                        <td id="advantage-technology" class="cell">
                            <h3>Освоение современных технологий для управления бизнес-процессами</h3>
                        </td>
                    </tr>

                    <tr>
                        <td id="advantage-education" class="cell">
                            <h3>Корпоративные программы обучения и развития профессиональных компетенций</h3>
                        </td>
                        <td id="advantage-growth" class="cell">
                            <h3>Профессиональный и карьерный рост</h3>
                        </td>
                    </tr>

                    <tr>
                        <td id="advantage-relations" class="cell">
                            <h3>Выстраивание отношений с крупнейшими отраслевыми игроками</h3>

                        </td>
                        <td id="advantage-wallet" class="cell">
                            <h3>Конкурентоспособная заработная плата на базе KPI</h3>
                        </td>
                    </tr>
                </table>

            </section>

            <section id="career-page-vacancies" class="wrapper">

                <h2>Вакансии</h2>

                <menu id="career-page-menu-1" class="career-page-menu">
                    <a href="#career-page-tech" class="active">Технические</a>
                    <a href="#career-page-logistics">Логистика</a>
                    <a href="#career-page-management">Менеджмент</a>
                    <a href="#career-page-finance">Финансы</a>
                    <a href="#career-page-control">Управление</a>
                </menu>

                <menu id="career-page-menu-2" class="career-page-menu">
                    <a href="#career-page-account">Бухгалтерия</a>
                    <a href="#career-page-it">IT-специалисты</a>
                    <a href="#career-page-security">Безопасность</a>
                </menu>

                <ul class="vacancies">

                    <li id="vacancy-engineer" class="vacancy">

                        <h3 class="title">Инженер-строитель</h3>

                        <section class="teaser">
                            <p>Производственно-инжиниринговая компания, которая специализируется на изготовлении
                                железнодорожного оборудования приглашает на работу инженера-строителя</p>
                        </section>

                        <section class="description">

                            <h4>Обязанности</h4>
                            <ul>
                                <li>Работа с технической документацией</li>
                                <li>Работа с подрядными организациями</li>
                                <li>Составление линейных графиков</li>
                            </ul>

                            <h4>Требования</h4>
                            <ul>
                                <li>Высшее строительное образование</li>
                                <li>Опыт работы от 5 лет</li>
                                <li>Знание порядка разработки и оформления технической документации, составление
                                    смет
                                </li>
                                <li>Умение вести технические и деловые переговоры</li>
                            </ul>

                            <h4>Условия</h4>
                            <ul>
                                <li>Офис в Сормовском районе</li>
                                <li>График работы: 5/2, время работы: с 8.00-17.00</li>
                                <li>Оформление по ТК РФ, оплачиваемый отпуск, больничный</li>
                                <li>Уровень дохода: 40 000 рублей</li>
                            </ul>

                            <div class="show-resume">
                                <a href="#show-form" class="button transparent">Отправить резюме</a>
                            </div>


                        </section>

                        <section class="resume hidden">

                            <h3>Резюме <span><a href="#" class="hide">Скрыть</a></span></h3>

                            <form action="" id="vacancy-form-engineer" name="vacancy-form" class="vacancy-form form">

                                <div class="field">
                                    <label for="vacancy-form-1-name">Имя и фамилия</label>
                                    <input type="text" name="name" id="vacancy-form-name" placeholder="">
                                </div>

                                <div class="field">
                                    <label for="vacancy-form-name">Телефон</label>
                                    <input type="text" name="name" id="vacancy-form-phone"
                                           placeholder="+7 999 999-99-99">
                                </div>

                                <div class="field">
                                    <label for="vacancy-form-name">Комментарий</label>
                                        <textarea name="vacancy-form-comment"
                                                  placeholder="Расскажите о себе в двух словах"></textarea>
                                </div>

                                <div class="field">
                                    <label for="vacancy-form-file">Резюме</label>
                                    <input type="text" name="vacancy-form-file">
                                    <button name="vacancy-form-browse">Обзор</button>
                                </div>

                                <div class="actions">
                                    <input type="submit" value="Отправить →">
                                </div>

                                <div class="confirmation hidden">

                                    <div class="wrapper">

                                        <h2 class="title">Ваше резюме принято!</h2>

                                        <p>Спасибо!</p>

                                        <p>Мы приняли ваше резюме, обработаем его и в ближайшее время свяжемся с вами.
                                            <br>
                                            Обычно это занимает от одного до трёх рабочих дней.</p>

                                        <p>А пока, вы можете узнать больше о нас и нашей компании.</p>

                                        <br>

                                        <p>
                                            <a href="/catalog/" class="button transparent">Продукция →</a>
                                            <a href="/about/" class="button">О компании →</a>
                                        </p>

                                    </div>

                                </div>

                            </form>

                        </section>

                        <div class="controls">
                            <a href="#" class="show">Посмотреть описание</a>
                            <a href="#" class="hide hidden">Скрыть описание</a>
                        </div>

                    </li>

                    <li id="vacancy-montirovshik" class="vacancy">

                        <h3 class="title">Монтировщик</h3>

                        <section class="teaser">
                            <p>Обязанности: монтировать установки.</p>
                            <p>Требования: стаж 10 тысяч лет.</p>
                        </section>

                        <section class="description">

                            <p>Механизм власти, тем более в условиях социально-экономического кризиса, неравномерен.
                                Вместе с тем, политическая система приводит плюралистический бихевиоризм. Феномен
                                толпы
                                формирует гуманизм, указывает в своем исследовании К.Поппер. Важно иметь в виду, что
                                авторитаризм означает политический процесс в современной России.</p>

                        </section>

                        <div class="controls">
                            <a href="#" class="show">Посмотреть описание</a>
                            <a href="#" class="hide hidden">Скрыть описание</a>
                        </div>

                    </li>
                    <li id="vacancy-gruzchik" class="vacancy">

                        <h3>Грузчик</h3>

                        <section class="teaser">
                            <p>Обязанности: умение грузить грузы.</p>
                            <p>Требования: грузоподъёмность 10 тысяч тонн.</p>
                        </section>

                        <section class="description">

                            <p>Кризис легитимности приводит классический коллапс Советского Союза, отмечает автор,
                                цитируя К.Маркса и Ф.Энгельса. Еще Шпенглер в "Закате Европы" писал, что либерализм
                                символизирует механизм власти. Понятие тоталитаризма определяет
                                континентально-европейский тип политической культуры. Унитарное государство приводит
                                континентально-европейский тип политической культуры.</p>

                        </section>

                        <div class="controls">
                            <a href="#" class="show">Посмотреть описание</a>
                            <a href="#" class="hide hidden">Скрыть описание</a>
                        </div>


                    </li>

                </ul>


            </section>

        </main>

        <aside id="contact-block" class="one-quarter unit">

            <div class="wrapper">

                <h2 class="title">Контакт</h2>

                <div class="grid">
                    <div class="two-fifths unit">
                        <div class="image frame">
                            <img src="/upload/career/petrova.png" alt="Светлана Сергеевна Петрова">
                        </div>
                    </div>
                    <div class="three-fifths unit">
                        <p class="person">Светлана Сергеевна Петрова</p>
                        <p class="position">HR-специалист</p>
                    </div>
                </div>

                <p>
                    +7 999 298-65-43<br>
                    <a href="mailto:hr@boostwagen.ru">hr@boostwagen.ru</a><br>
                    skype: <a href="skype:hr.boost">hr.boost</a>
                </p>

                <div id="contact-form-wrapper">

                    <div id="contact-form-show">
                        <a href="#contact-form" class="button transparent wide">Задать вопрос</a>
                    </div>

                    <div id="contact-form-container" class="hidden">

                        <form action="" id="contact-form" name="contact-form" class="form">

                            <div class="field">
                                <label for="contact-form-question">Задайте вопрос</label>
                                <textarea name="contact-form-question" id="contact-form-question"></textarea>
                            </div>

                            <div class="field">
                                <label for="contact-form-phne">Укажите контакт для связи</label>
                                <input name="contact-form-phone" id="contact-form-phone">
                            </div>

                            <div class="actions">
                                <input type="submit" class="transparent" value="Отправить">
                            </div>

                        </form>

                    </div>

                    <div id="contact-form-confirmation" class="hidden">
                        <p>Спасибо, ваш вопрос отправлен!</p>
                        <p>Мы ответим по телефону или почте, указанным в профиле.</p>
                    </div>

                </div>

            </div>

        </aside>

    </div>

</div>

<? include 'includes/footer.php'; ?>

</body>

</html>