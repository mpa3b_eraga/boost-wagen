<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<meta charset="UTF-8">

<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
<link rel="icon" type="image/x-icon" href="/favicon.ico"/>

<link href="/css/normalize.css" type="text/css" rel="stylesheet"/>
<link href="/css/grid.css" type="text/css" rel="stylesheet"/>
<link href="/css/font.css" type="text/css" rel="stylesheet"/>
<link href="/css/common.css" type="text/css" rel="stylesheet"/>
<link href="/css/layout.css" type="text/css" rel="stylesheet"/>

<script src="/js/css-ua.js"></script>

<script src="/lib/jquery/jquery.js"></script>

<script src="/js/common.js"></script>