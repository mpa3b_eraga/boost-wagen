<link rel="stylesheet" href="/lib/slick/slick.css">
<script src="/lib/slick/slick.js"></script>

<link rel="stylesheet" href="/css/front-slider.css">
<script src="/js/front-slider.js"></script>

<aside id="front-slider" class="wrap">

    <div class="grid">

        <div id="main-slider" class="whole unit slider">

            <div class="slide">
                <div class="grid">
                    <div class="two-fifths image unit">
                        <img src="/upload/notebook.png">
                    </div>
                    <div class="three-fifths caption unit">
                        <h2>Онлайн-заказ</h2>
                        <p>Удобный сервис для быстрой покупки железнодорожной продукции на нашем
                            сайте</p>
                        <p><a href="/catalog/" class="button">Перейти в каталог</a></p>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="grid">
                    <div class="two-fifths image unit">
                        <img src="/upload/tablet-and-phone.png">
                    </div>
                    <div class="three-fifths caption unit">
                        <h2>Работайте с каталогом
                            на любых устройствах</h2>
                        <p>Наш интерфейс адаптирован к стационарным ПК, ноутбукам,
                            планшетам и смартфонам</p>
                        <p><a href="/catalog/" class="button">Перейти в каталог</a></p>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="grid">
                    <div class="two-fifths image unit">
                        <div id="inner-slider">
                            <div class="slide">
                                <a href="/catalog.php/">
                                    <div class="frame">
                                        <img src="/upload/subslide-img-1.png">
                                        <div class="caption">
                                            <h3>Тормозная система
                                                <span>109.01.000-0-029</span>
                                            </h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="/catalog.php/">
                                    <div class="frame">
                                        <img src="/upload/subslide-img-2.png">
                                        <div class="caption">
                                            <h3>Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="/catalog.php/">
                                    <div class="frame">
                                        <img src="/upload/subslide-img-3.png">
                                        <div class="caption">
                                            <h3>Буксовый узел
                                                <span>106.01.000-0-0587</span>
                                            </h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="/catalog.php/">
                                    <div class="frame">
                                        <img src="/upload/subslide-img-2.png">
                                        <div class="caption">
                                            <h3>Автосцепка (СА-3)
                                                <span>106.01.000-0-05СБ</span>
                                            </h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="/catalog.php/">
                                    <div class="frame">
                                        <img src="/upload/subslide-img-3.png">
                                        <div class="caption">
                                            <h3>Буксовый узел
                                                <span>106.01.000-0-0587</span>
                                            </h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="three-fifths caption unit">
                        <h2>Широкий ассортимент</h2>
                        <p>Структура каталога и интерактивный поиск помогают быстро узнать
                            о наличии на складе необходимых комплектующих</p>
                        <p><a href="/catalog/" class="button">Посмотреть каталог</a></p>
                    </div>
                </div>
            </div>

            <div id="front-slider-advantages-slide" class="slide align-center">

                <h2>Преимущества покупки на сайте</h2>

                <div class="grid">

                    <div class="one-quarter unit">
                        <div class="image">
                            <img src="/upload/advantages/advantage-order.png">
                        </div>
                        <h3>24 / 7 /365</h3>
                    </div>
                    <div class="one-quarter unit">
                        <div class="image">
                            <img src="/upload/advantages/advantage-rouble.png">
                        </div>
                        <h3>Открытые цены</h3>
                    </div>
                    <div class="one-quarter unit">
                        <div class="image">
                            <img src="/upload/advantages/advantage-login.png">
                        </div>
                        <h3>Управление заказами</h3>
                    </div>
                    <div class="one-quarter unit">
                        <div class="image">
                            <img src="/upload/advantages/advantage-person.png">
                        </div>
                        <h3>Персональный подход</h3>
                    </div>

                </div>
            </div>

        </div>

    </div>

</aside>
