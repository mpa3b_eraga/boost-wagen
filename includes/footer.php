<link rel="stylesheet" href="/css/footer.css">
<script src="/js/footer.js"></script>

<footer id="footer" class="wrap">
    
    <div id="footer-navigation" class="grid">

        <div class="four-fifths unit">

            <nav>

                <ul class="menu grid">

                    <li class="item one-quarter unit column-1">
                        <a href="/about/">О компании</a>
                        <ul>
                            <li><a href="/about/model">Модель бизнеса</a></li>
                            <li><a href="/about/partners">Партнёры</a></li>
                        </ul>
                    </li>

                    <li class="item one-quarter unit column-2">
                        <a href="/catalog/">Каталог продукции</a>
                        <ul>
                            <li><a href="/catalog/online-order/">Онлайн-заказ</a></li>
                            <li><a href="/user/">Личный кабинет</a></li>
                            <li><a href="/catalog/special/">Спецпредложения</a></li>
                        </ul>
                    </li>

                    <li class="item one-quarter unit column-3">
                        <a href="/storage/">Складская логистика</a>
                        <ul>
                            <li><a href="/storage-logistics/storage/">Ответственное хранение</a></li>
                            <li><a href="/storage-logistics/process/">Складская обработка</a></li>
                            <li><a href="/storage-logistics/it/">Информатизация</a></li>
                            <li><a href="/storage-logistics/quality-control/">Контроль качества</a></li>
                        </ul>
                    </li>

                    <li class="item one-quarter unit column-4">
                        <a href="/help/">Центр помощи</a>
                        <ul>
                            <li><a href="/help/advantages/">Преимущества личного кабинета</a></li>
                            <li><a href="/help/instuctions/">Инструкции</a></li>
                        </ul>
                    </li>

                </ul>

            </nav>

        </div>

        <div class="one-fifth unit">

            <ul class="menu grid">
                <li class="item whole unit">
                    <a href="/suppliers/login/">Вход для поставщиков</a>
                    <ul>
                        <li class="item">Boostwagen © 2016</li>
                        <li class="item">Разработка сайта — <a href="http://www.xdesign-nn.ru/"
                                                               target="_blank">xDesign</a></li>
                    </ul>
                </li>
            </ul>

        </div>

    </div>
    
    <div id="footer-outro" class="grid">
    
        <div class="four-fifths unit">
        
            <nav>
            
                <ul class="menu grid">
                
                    <li class="item one-quarter unit column-1">
                        <a href="/carrier/">Карьера</a>
                        <ul>
                            <li><a href="/carrier/vacancy/">Вакансии</a></li>
                        </ul>
                    </li>
                
                    <li class="item one-quarter unit column-2">
                        <form action="" id="subscription-form" class="inline-form">
                            <span class="field email">
                                <input type="email" placeholder="example@mail.ru">
                            </span>
                            <p class="small description">Подписка на обновление цен</p>
                        </form>
                    </li>
                
                    <li class="item one-quarter unit column-3">
                        <a href="/transport/">Транспортная логистика</a>
                        <ul>
                            <li><a href="/transport/wages/">Транспортировка грузов</a></li>
                            <li><a href="/transport/customs/">Таможенное оформление</a></li>
                        </ul>
                    </li>
                
                    <li class="item one-quarter unit column-4">
                        <a href="/documents/">Документы</a>
                    </li>
            
                </ul>
            
                <ul class="menu grid">
                
                    <li class="item one-quarter unit column-1">
                        <a href="/contact/">Контакты</a>
                    </li>
                
                    <li class="item one-quarter unit column-2">
                        <a href="/dealer/">Стать дилером</a>
                    </li>
            
                </ul>
        
            </nav>
    
        </div>
    
        <div class="one-fifth unit">
        
            <form action="" id="search-form" class="inline-form">
                <input type="text" placeholder="Найти...">
            </form>
    
        </div>


    </div>

</footer>