<link rel="stylesheet" href="/css/header.css">

<header id="header" class="wrap">

    <div class="grid">

        <div id="header-logo" class="one-quarter unit">
            <a href="/">
                <img src="/img/logo.png" alt="Boostwagen">
            </a>
        </div>
    
        <div id="header-navigation" class="three-quarters unit align-right">

            <div id="header-phone" class="element">
                <a href="tel:88006526532">8 800 652-65-32</a>
            </div>

            <div id="header-language" class="element">
                <a class="active" href="#">RU</a>
                <a href="#">EN</a>
            </div>

            <script src="/js/header-user.js"></script>
            <link rel="stylesheet" href="/css/header-user.css">

            <ul id="header-user" class="element menu">

                <li>

                    <a href="#header-guest-dropdown">личный кабинет</a>

                    <ul id="header-guest-dropdown" class="dropdown">

                        <h3 class="title">Зарегистрируйтесь или <a href="#header-user-dropdown">войдите</a></h3>

                        <form id="login-form" action="" class="form">

                            <div class="field">
                                <label for="login-form-email">Введите свою эл.почту</label>
                                <input id="login-form-email" type="email" name="login-form-email"
                                       placeholder="Например, info@rzd.com">
                            </div>

                            <div class="field">
                                <label for="login-form-password">Придумайте пароль</label>
                                <input id="login-form-password" type="text" name="login-form-password" placeholder="">
                            </div>

                            <div class="actions">
                                <input type="submit" value="Зарегистрироваться">
                            </div>

                        </form>

                    </ul>

                    <ul id="header-user-dropdown" class="dropdown">

                        <h3 class="title">Войдите или <a href="#header-guest-dropdown">зарегистрируйтесь</a></h3>

                        <form id="auth-form" action="" class="form">

                            <div class="field">
                                <input id="login-form-email" type="email" name="login-form-email"
                                       placeholder="Например, info@rzd.com">
                            </div>

                            <div class="field">
                                <input id="login-form-password" type="password" name="login-form-password"
                                       placeholder="">
                            </div>

                            <div class="actions">
                                <input type="submit" value="Войти">
                            </div>

                        </form>

                    </ul>

                </li>

            </ul>

            <script src="/lib/nicescroll/jquery.nicescroll.js"></script>
            <script src="/js/header-cart.js"></script>
            <link rel="stylesheet" href="/css/header-cart.css">

            <ul id="header-cart" class="menu element">

                <li>

                    <span class="value">12</span>

                    <a href="#header-cart-dropdown">заказ</a>

                    <ul id="header-cart-dropdown" class="hidden align-left">

                        <h3>В заказе 25 товаров:</h3>

                        <p><a href="/cart/">Перейти в корзину</a> →</p>

                        <ul class="items">

                            <li class="grid item">
                                <div class="two-fifths unit">
                                    <img src="/upload/header-cart/item-1.png" alt="">
                                </div>
                                <div class="three-fifths unit">
                                    <h4>Замкодержатель<br>
                                        106.01.000-0-05СБ</h4>
                                    <p>
                                        <span class="quantity">5 шт.</span>
                                        <span class="price">150 000<i class="rouble"></i></span>
                                    </p>
                                </div>
                            </li>

                            <li class="grid item">
                                <div class="two-fifths unit">
                                    <img src="/upload/header-cart/item-2.png" alt="">
                                </div>
                                <div class="three-fifths unit">
                                    <h4>Валик подъёмника <br>
                                        106.01.000-0-05СБ</h4>
                                    <p>
                                        <span class="quantity">4 шт.</span>
                                        <span class="price">14 000<i class="rouble"></i></span>
                                    </p>
                                </div>
                            </li>

                            <li class="grid item">
                                <div class="two-fifths unit">
                                    <img src="/upload/header-cart/item-3.png" alt="">
                                </div>
                                <div class="three-fifths unit">
                                    <h4>Автосцепка<br>
                                        106.01.000-0-05СБ</h4>
                                    <p>
                                        <span class="quantity">3 шт.</span>
                                        <span class="price">1 204 000<i class="rouble"></i></span>
                                    </p>
                                </div>
                            </li>

                            <li class="grid item">
                                <div class="two-fifths unit">
                                    <img src="/upload/header-cart/item-4.png" alt="">
                                </div>
                                <div class="three-fifths unit">
                                    <h4>Подъёмник замка<br>
                                        106.01.000-0-05СБ</h4>
                                    <p>
                                        <span class="quantity">10 шт.</span>
                                        <span class="price">7 000<i class="rouble"></i></span>
                                    </p>
                                </div>
                            </li>

                        </ul>

                    </ul>

                </li>

            </ul>
    
            <script src="/js/sidemenu.js"></script>
            <link rel="stylesheet" href="/css/side-menu.css">
    
            <div id="side-menu" class="element">
                <a href="#" class="control"></a>
        
                <div id="side-menu-bar">
            
                    <a class="close" href="#"></a>
            
                    <nav>
                        <ul class="menu">
                            <li><a href="/catalog.php">Каталог продукции</a></li>
                            <li><a href="#">Складская логистика</a></li>
                            <li><a href="#">Траснспортная логистика</a></li>
                        </ul>
                        <ul class="menu">
                            <li><a href="#">Компания</a></li>
                            <li><a href="/contact.php">Контакты</a></li>
                        </ul>
                    </nav>
        
                </div>
    
            </div>

        </div>

    </div>

</header>