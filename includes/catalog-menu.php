<aside id="sidebar-left" class="one-quarter unit">

    <link rel="stylesheet" href="/css/catalog-menu.css">
    <script src="/lib/sticky-kit/jquery.sticky-kit.js"></script>
    <script src="/js/catalog-menu.js"></script>

    <div class="frame">

        <nav id="catalog-menu">

            <ul class="menu">
                <li class="section collapsed">
                    <a href="#">Автосцепное устройство</a>

                    <ul>

                        <li class="section collapsed">
                            <a href="#">Автосцепка (СА-3) и комплектующие</a>

                            <ul>
                                <li><a href="#">Автосцепка (СА-3) 106.01.000-0-05СБ</a></li>
                                <li><a href="#">Замок автосцепки ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</a></li>
                                <li><a href="#">Замкодержатель 106.01.000-0-05СБ</a></li>
                                <li><a href="#">Подъемник замка ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</a></li>
                                <li><a href="#">Предохранитель замка 106.01.000-0-05СБ</a></li>
                                <li><a href="#">Хомут тяговый ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</a></li>
                                <li><a href="#">Клин тягового хомута 106.01.000-0-05СБ</a></li>
                                <li><a href="#">Балочка центрирующая ЧЛЗ.ПКБ.ЦВ-106.00.001-1/-2ЛУ</a></li>
                            </ul>

                        </li>

                        <li class="section">
                            <a href="#">Детали автосцепного устройства</a>
                        </li>

                    </ul>

                </li>
                <li class="section collapsed"><a href="#">Буксовый узел</a></li>
                <li class="section collapsed"><a href="#">Поглощающие аппараты</a></li>
                <li class="section collapsed"><a href="#">Проект М1698 </a></li>
                <li class="section collapsed"><a href="#">Детали кузова вагона</a></li>
                <li class="section collapsed"><a href="#">Тормозная система вагона</a></li>
                <li class="section collapsed"><a href="#">Триангель</a></li>
                <li class="section collapsed"><a href="#">Тележка 18-9855 Барбер</a></li>
                <li class="section collapsed"><a href="#">Тележка 18-194-1</a></li>
                <li class="section collapsed"><a href="#">Тележка 18-578</a></li>
                <li class="section collapsed"><a href="#">Тележка 18-100</a></li>
                <li class="section collapsed"><a href="#">Спецпредложения</a></li>
            </ul>

        </nav>

        <aside id="catalog-search">
            <form id="catalog-search-form" action="" class="form">
                <input id="catalog-search-form-input" type="text" placeholder="Поиск товара">
            </form>
        </aside>

    </div>

</aside>